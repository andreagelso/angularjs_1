var app = angular.module("myApp", ["ngRoute"]); //dependecies

app.config(function($routeProvider) { // services
    $routeProvider
    .when("/", {
        templateUrl : "main.htm",
    })
    .when("/london", {
        templateUrl : "pages/london/london.htm",
        controller : "londonCtrl"
    })
    .when("/paris", {
        templateUrl : "pages/paris/paris.htm",
        controller : "parisCtrl"
    })
    .when("/paris/:user_id", {
        templateUrl : "pages/paris/parisdetails.htm",
        controller : "parisCtrl"
    })
    .otherwise({
      templateUrl: '404.html',
        controller: function ($scope) {
            $scope.message = 'Error Page';
        }
    });
});
