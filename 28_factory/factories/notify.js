app.factory('notify',   function() {
   var msgs = [];

   return function(msg) {
     msgs.push(msg);
     if (msgs.length == 3) {
       alert(msgs.join('\n'));
       msgs = [];
     }
    };
 });