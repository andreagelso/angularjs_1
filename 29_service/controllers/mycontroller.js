var myApp = angular.module('myApp', ['CalculatorService']);
myApp.controller('myController', function ($scope, Calculator) {

    $scope.findSquare = function () {
        $scope.answer = Calculator.square($scope.number);
    }
});

