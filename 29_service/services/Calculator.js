var CalculatorService = angular.module('CalculatorService', [])

    CalculatorService.service('Calculator', function () {
        this.x = 0;

        this.square = function (a) {
            this.x = a * a;
            return this.x;
        };

        this.get = function () {
            return this.x;
        };

    }
);